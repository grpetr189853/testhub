<?php

namespace frontend\controllers;

use common\models\TestsAnswers;
use common\models\TestsQuestions;
use common\models\TestType;
use common\models\User;
use common\models\UsersAnswers;
use common\models\UsersTests;
use Test;
use Yii;
use common\models\Tests;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TestsController implements the CRUD actions for Tests model.
 */
class TestsController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tests models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user_id = Yii::$app->user->identity->id;
        $usersTests = UsersTests::findAll(['user_id' => $user_id]);
        $test_ids = [];
        foreach ($usersTests as $userTest){
            array_push($test_ids, $userTest->test_id);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => Tests::find()->where(['id' => $test_ids]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tests model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tests model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tests();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tests model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tests model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tests::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * The action is performed when the student starts the test.
     * If successful, the browser will be redirected to the first question of the test.
     * @param integer $id
     * @return mixed
     */
    public function actionInit($id)
    {
        // Init the test and reset the test results
        $studentTest = UsersTests::find()->where('test_id=:testId', [
            'testId' => $id
        ])->one();
        $studentTest->attempts -= 1;
        $studentTest->start_time = date('Y-m-d H:i:s');
        $studentTest->end_time = null;
        try {
            $studentTest->update();
        } catch (StaleObjectException $e) {
            } catch (\Throwable $e) {
        }
        $answerModel = UsersAnswers::find()->where('test_id=:testId AND user_id=:userId',[
            'testId'     => $id,
            'userId'     => \Yii::$app->user->getId(),
        ])->all();
        foreach ($answerModel as $answer){
            try {
                $answer->delete();
            } catch (StaleObjectException $e) {
                } catch (\Throwable $e) {
            }
        }
        return $this->redirect(array(
            'process',
            'id' => $id
        ));

    }


    /**
     * Performs the test.
     * @param integer $id
     * @param null $q
     * @return mixed
     * @throws \yii\base\ExitException
     * @throws \yii\web\HttpException
     */
    public function actionProcess($id, $q = null) {
        $userAnswers = Yii::$app->request->post('UsersAnswers');
        if(isset($userAnswers)){
            /**
             * Before accepting an answer for verification, we check if the time has passed for the test.
             * If the time is up, we will redirect to the page with the result.
             */
            if (! (new Tests)->checkTestTimeLimit(Yii::$app->request->post('testStartTime'), Yii::$app->request->post('testTimeLimit'))) {
                Yii::$app->session->set('endTest', true);
                if (Yii::$app->request->isAjax) {
                    echo json_encode([
                        'redirect' => Yii::$app->urlManager->createUrl('test/result', [
                            'id' => Yii::$app->request->post('testId'),
                        ])
                    ]);

                    Yii::$app->end();
                } else {
                    $this->redirect(array(
                        'result',
                        'id' => Yii::$app->request->post('testId'),
                    ));
                }
            }

            /**
             * checks if the answer to the question exists - and if it does, it updates it; if it doesn't exist, it stores it in the database
             */
            $answerModel = new UsersAnswers();


            $studentAnswer = ($answerModel)->getAnswerModel(Yii::$app->request->post('UsersAnswers')['question_id'], Yii::$app->request->post('UsersAnswers')['scenario'], Yii::$app->request->post('testId'));

            $studentAnswer->attributes = Yii::$app->request->post('UsersAnswers');

            if (isset(Yii::$app->request->post('UsersAnswers')['selectedAnswers'])) {
                $studentAnswer->selectedAnswers = Yii::$app->request->post('UsersAnswers')['selectedAnswers'];
            }

            if ($studentAnswer->validate()) {
                $studentAnswer->save(false);
                echo JSON::encode(array(
                    'validateStatus' => 'success'
                ));
            } else {
                var_dump($studentAnswer->getErrors());
            }

            Yii::$app->end();
        }

        $test = Tests::find()->where('id=:testId',['testId'=> $id])->one();//TODO: заменить методом loadModel()
        $test_questions = TestsQuestions::find()->where('test_id=:testId',['testId'=> $id])->all();

        $directQuestionNumber = 1;

        if ($directQuestionNumber = Yii::$app->request->get('q')) {
            if ($directQuestionNumber === 'end') {
                $this->redirect([
                    'process',
                    'id' => $test->id
                ]);
            }
        } else {
            $directQuestionNumber = 1;
        }

        if ($directQuestionNumber > count($test_questions)) {
            throw new \yii\web\HttpException(404,'Not Found.');
        }

        $studentTest = UsersTests::find()->where('test_id=:testId', ['testId' => $id])->one();

        $questionDataArray = array(); //An array containing the necessary information about all questionsх
        $questionNumberIdPair = array(); // Array question number => question ID

        foreach ($test_questions as $key => $question) {
            $questionDataArray[$key + 1] = array(
                'id' => $question->id,
                'title' => $question->question_text,
                'type' => TestType::find()->where('id=:questionType',[
                    'questionType' => $question->question_type,
                ])->one()->type,
                'answerIdTextPair' => array(ArrayHelper::map($question->getTestsAnswers()->all(),'id','answer_text')),// The array contains a pair of answer option ID => Answer option textа
            );
            $questionNumberIdPair[$key + 1] = $question->id;
        }

        $answerModel = new UsersAnswers();

        return $this->render('test_starter',[
            'tests_questions'  => $test_questions,
            'test'         => $test,
            'directQuestionNumber' => $directQuestionNumber,
            'answerModel' => $answerModel,
            'questionDataArray' => $questionDataArray,
            'questionNumberIdPair' => $questionNumberIdPair,
            'testTimeLimit' => $test->time_limit * 60,
            'testStartTime' => strtotime($studentTest->start_time)
        ]);
    }

    /**
     * If the test has not yet been run or is re-running, the action counts
     * test result and brings the record in the UsersTests table to the appropriate form
     * (assigns values to the result and end_time columns). Otherwise, just print the result of
     * test execution.
     * @param $id
     * @return mixed
     */
    public function actionResult($id)
    {
        $studentTest = UsersTests::find()->where('test_id=:testId AND user_id=:userId', [
            'testId' => $id,
            'userId' => Yii::$app->user->identity->id,
        ])->one();
        $end_test = Yii::$app->request->post('endTest');
        if($studentTest){
            if(isset($end_test) || Yii::$app->session->get('endTest') === true){
                $studentTotalScore = 0;
                $studentAnswers = UsersAnswers::find()->where('test_id=:testId AND user_id=:userId',[
                    'testId' => $id,
                    'userId' => Yii::$app->user->identity->id,
                ])->all();
                foreach ($studentAnswers as $answer){
                    $studentTotalScore += $answer->result_points;
                }
                $studentTest->result = $studentTotalScore;
                try {
                    $studentTest->update();
                } catch (StaleObjectException $e) {
                    } catch (\Throwable $e) {
                }

                $message = 'Тест был успешно сдан.';
                $testPassed = true;

                if ($studentTotalScore < $studentTest->test->minimum_score) {
                    $message = 'Вы набрали недостаточно баллов для прохождения теста';
                    $testPassed = false;
                }

                return $this->render('test_result',[
                    'totalScore' => $studentTotalScore,
                    'studentTest' => $studentTest,
                    'message' => $message,
                ]);
            }

        }

    }

    /**
     * The action only allows POST requests and is intended to dynamically display the requested question.
     * @return mixed
     */
    public function actionPostQuestion()
    {
        if (isset($_POST) && isset($_POST['questionNumber']) && isset($_POST['questionNumberIdPair'])) {
            $questionNumber = $_POST['questionNumber'];
            $numberOfQuestions = count($_POST['questionNumberIdPair']);

            $questionDataArray = array();
            $studentAnswersQuestionId = array();
            $answerModel = null;

            if (Yii::$app->request->post('questionDataArray')) {
                $questionDataArray = $_POST['questionDataArray'];
            }

            $studentAnswersQuestionId = Tests::getStudentAnswersByQuestionsId($_POST['questionNumberIdPair']);

            $questionAlert = '';

            if ($questionNumber == 'end') {
                $questionAlert = 'Завершить тест?';

                if (count($studentAnswersQuestionId) < $numberOfQuestions) {
                    $questionAlert = 'Вы ответили не на все вопросы. ' . $questionAlert;
                }
            }
            $answerModel = new UsersAnswers();
            return $this->renderPartial('test_question', array(
                'answerModel' => $answerModel,
                'testID' => $_POST['testID'],
                'questionNumber' => $questionNumber,
                'questionNumberIdPair' => $_POST['questionNumberIdPair'],
                'questionDataArray' => $questionDataArray,
                'questionAlert' => $questionAlert,
                'numberOfQuestions' => $numberOfQuestions,
                'studentAnswersQuestionId' => $studentAnswersQuestionId,
                'testTimeLimit' => $_POST['testTimeLimit'],
                'testStartTime' => $_POST['testStartTime']
            ));
        }
    }
}

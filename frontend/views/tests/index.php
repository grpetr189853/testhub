<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tests-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description',
            'category_id',
            'question_count',
            //'minimum_score',
            //'time_limit:datetime',
            //'attempts',
            //'create_time',
            //'deadline',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete} {startTest}',
                    'buttons' => [
                        'startTest' => function ($url,$model,$key) {
                            return Html::a('Начать тест', Url::to(['/tests/init', 'id' => $model->id]), ['class' => 'btn btn-success btn-xs']);
                        },
                    ],

            ],
        ],
    ]); ?>


</div>

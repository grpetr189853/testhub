<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="container th-container">
    <div class="navbar navbar-default th-navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <?php
                NavBar::begin([
                    'options' => [
                        'class' => 'navbar-nav navbat-fixed-top',
                    ],
                ]);
                $menuItems[] = ['label' => Html::img(Yii::getAlias('@web').'/img/logo.png' ), 'encode' => false,'url' => '/'];

                if (Yii::$app->user->isGuest) {
                    $menuItems[] = [
                        'label' => 'Зарегистрироваться',
                        'items' => [
                            ['label' => 'Преподаватель', 'url' => ['/site/signup-teacher']],
                            '<li class="divider"></li>',
                            ['label' => 'Студент', 'url' => ['/site/signup-student']],
                        ],
                        'options' => ['class' => 'menu'],
                    ];
                    $menuItems[] = ['label' => 'Пройти тесты', 'url' => ['/site/login'], 'options' => ['class' => 'menu'],];
                    $menuItems[] = ['label' => 'Создать тесты', 'url' => Yii::$app->urlManagerBackend->createUrl(['backend/web/site/login']), 'options' => ['class' => 'menu'],];
                } else {
                    $menuItems[] = '<li class="menu">'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';
                }

                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-left'],
                    'items' => $menuItems,
                ]);
                NavBar::end();
                ?>
            </div>
        </div>
        <!--/.container-fluid-->
    </div>
    <!--/.navbar-->
    <div class="jumbotron th-jumbotron">
        <div class="navbar-collapse collapse menu">
            <div class="icon-close">

            </div>

        </div>

        <?php echo $content; ?>
    </div>
</div>


<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

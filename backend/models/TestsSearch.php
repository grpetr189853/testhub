<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tests;

/**
 * TestsSearch represents the model behind the search form of `common\models\Tests`.
 */
class TestsSearch extends Tests
{
    public $categoryName;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'question_count', 'minimum_score', 'time_limit', 'attempts'], 'integer'],
            [['name', 'description', 'create_time', 'deadline','categoryName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tests::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category'  => $this->categoryName,
            'question_count' => $this->question_count,
            'minimum_score' => $this->minimum_score,
            'time_limit' => $this->time_limit,
            'attempts' => $this->attempts,
            'create_time' => $this->create_time,
            'deadline' => $this->deadline,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}

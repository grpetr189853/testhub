<?php
use backend\components\DynamicTabularForm\DynamicTabularForm;
use yii\helpers\Html;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

    $form = DynamicTabularForm::begin(array(
        'defaultRowView'=>'question_form',
        'id'=>'test-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
    ));
    

    //var_dump(strtotime('2015-02-15 21:00:00'), $test->deadline);
?>

<div class="form th-test-from ignore-mathjax">
    <div class="test-fields">

    <div class="row">
            <?= $form->field($test,'name')->textInput()->hint('Название теста',['class' => 'test-form-hint'])->label('Название',['class' => 'test-form-label'])?>
	</div>
    
        <div class="row">
            <?= $form->field($test, 'description')->widget(Widget::className(), [
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 200,
                        'imageUpload' => Url::to(['/default/image-upload']),
                        'plugins' => [
                            'clips',
                            'fullscreen',
                            'imagemanager',
                        ],
                    ],
                ])->hint('Короткое описание теста',['class' => 'test-form-hint'])->label('Описание',['class' => 'test-form-label']) ?>
        </div>
        <div class="row">
            <?= $form->field($test, 'category_id')->dropDownList(ArrayHelper::map(\common\models\TestsCategory::find()->all(), 'id', 'name'))->hint('Категория теста',['class' => 'test-form-hint'])->label('Категория',['class' => 'test-form-label']) ?>
        </div>
        <div class="row">
            <?= $form->field($test, 'question_count')->textInput()->hint('Количество вопросов',['class' => 'test-form-hint'])->label('Количество вопросов',['class' => 'test-form-label']) ?>
        </div>    
	<div class="row">
            <?= $form->field($test, 'minimum_score')->textInput()->hint('Минимальный балл для прохождения теста',['class' => 'test-form-hint'])->label('Минимальный балл',['class' => 'test-form-label']) ?>
        </div>

	<div class="row">
            <?= $form->field($test, 'time_limit')->textInput()->hint('Время на прохождение теста (мин)',['class' => 'test-form-hint'])->label('Время на прохождение теста (мин)',['class' => 'test-form-label']) ?>
	</div>

	<div class="row">
            <?= $form->field($test, 'attempts')->textInput()->hint('Количество попыток',['class' => 'test-form-hint'])->label('Попыток',['class' => 'test-form-label']) ?>
	</div>
        <div class="row">
            <?= $form->field($test, 'deadline')->textInput()->hint('Срок сдачи',['class' => 'test-form-hint'])->label('Срок сдачи',['class' => 'test-form-label']) ?>
        </div>
    </div>
<?php
    echo $form->myRow($questions);
?>

<div class="test-create-wrapper">
<?php echo Html::submitButton($pageLabel, array('class' => 'create-test-button th-submit-button')); ?>
<div class="qeustions-empty-error"></div> 
</div>
    <?php DynamicTabularForm::end(); ?>


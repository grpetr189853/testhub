<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Tests */

$this->title = 'Создать тест';
$this->params['breadcrumbs'][] = ['label' => 'Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if ($test->scenario === 'insert') {
    $pageLabel = 'Создать тест';
}

if ($test->scenario === 'update') {
    $pageLabel = 'Изменить тест';
}
$newOptionUrl = \Yii::$app->urlManager->createUrl('tests/option-field');
?>
<div class="tests-create">

    <h1 class="first-header"><?= Html::encode($this->title) ?></h1>

<script>
function Preview(classPreview, classBuffer, classTextField) {
	this.preview = $('.'+classPreview)[0];
    this.buffer = $('.'+classBuffer)[0];
    this.classTextField = classTextField;
    this.delay = 150;        // задержка перед обновлением и после нажатия клавиши
    this.timeout = null;     // setTimout id
    this.mjRunning = false;  // true есои MathJax обрабатывает полученные данные
    this.oldText = null;

    //  Переключение между buffer div - preview div и выбор нужного в заисимости от наличия 
    //  изменений в редакторе.

    this.SwapBuffers = function () {
        var buffer = this.preview, preview = this.buffer;
        this.buffer = buffer; this.preview = preview;
        buffer.style.visibility = "hidden"; buffer.style.position = "absolute";
        preview.style.position = ""; preview.style.visibility = "";
    };

    //  Метод будет вызван при нажатии клавиши в редакторе.
    //  Апдейт будет произведен после небольшого дилэя между нажатиями клавишь,
    //  что позволяет избежать ненужных обращений к MathJax при последовательном наборе.

    this.Update = function () {
        if (this.timeout) {clearTimeout(this.timeout)}
        this.timeout = setTimeout(this.callback,this.delay);
    };
    
    //
    //  Создает превью и запускает MathJax для его обработки.
    //  Если MathJax уже пытается отрендерить код, return.
    //  Если текст в редакторе не изменился, return.
    //  После обработки текста MathJax-ом вызываем PreviewDone.
    
    this.CreatePreview = function () {
    	this.timeout = null;
        if (this.mjRunning) return;
        var textElement = $('.' + this.classTextField);
        if(textElement.prop('tagName') === 'INPUT') {
        	var text = textElement.val();
        } else {
        	var text = textElement.html();
        }
        
        if (text === this.oldtext) return;
        this.buffer.innerHTML = this.oldtext = text;
        this.mjRunning = true;
        MathJax.Hub.Queue(
          ["Typeset",MathJax.Hub,this.buffer],
          ["PreviewDone",this]
        );
    };
    
    this.PreviewDone = function () {
        this.mjRunning = false;
        this.SwapBuffers();
    }
}

function showAnswerOptionPreview(element) {
    var questionNumber = $(element).attr('data-question-number');

    var optionNUmber = $(element).attr('data-option-number');

    var optionFieldWidth = $('.answer-text-area-'.concat(questionNumber, '-', optionNUmber)).css('width');
    var optionFieldHeight = $('.answer-text-area-'.concat(questionNumber, '-', optionNUmber)).css('height');
	
	var hideContainerStyles = {
		"visibility": "hidden",
		"position": "absolute"
	};
		
	var showContainerStyles = {
		"visibility": "",
		"position": "",
	};

	var optionContainer = $('.option-preview-container-'.concat(questionNumber, '-', optionNUmber));

	if(optionContainer.css('visibility') === 'hidden') {
		optionContainer.css(showContainerStyles);
	} else {
		optionContainer.css(hideContainerStyles);
	}	
}
<?php
$this->registerJs("
// Функция добавляет поле для ввода варианта ответа

function addOption(input){
    var parentClass = $(input).attr('data-add');

    var key = parentClass.match(/\d+$/);

    // <div>, который содержит все варианты ответов
    var parent = $('.'+parentClass);

    // Вычисляем номер следующего варианта ответа
    var answerOptionNumber = parent.children('.row').length + 1;

    var optionIdArray = new Array();
    var newOptionId;

    $('.'+parentClass+' div[class^=row]').each(function () {
        var optionId = $(this).attr('class').match(/answer-option-(\d+)/);
        optionIdArray.push( optionId[1] );
    });

    if(optionIdArray.length === 0) {
        newOptionId = 1;
    } else {
        newOptionId = Math.max.apply(Math, optionIdArray) + 1;
    }

    // GET запрос на экшен, который рендерит текстовое поле для ввода варианта
    $.ajax({
        url:'" . $newOptionUrl . "',
        data:{
            i:newOptionId,
            key:key[0],
            number:answerOptionNumber
        },
        cache: false,
        dataType: 'html',
        success:function(data){
            parent.append(data);
        },
    });
};

// Функция удаляет поле варианта ответа и изменяет номера вариантов в соответствии с их количеством

function deleteOption(input){

    // <div>, который содержит все варианты ответов
    //var optionsContainer = $(input).parents().eq(1);
    var optionsContainer = $('.js-options-'.concat($(input).attr('data-question-number')));

    // Удаляем указанный <div>
    $(input).parents('.answer-option-'.concat($(input).attr('data-option-number'))).remove();
    //$(input).parent().remove();

    // Вычисляем класс optionsContainer
    var optionsContainerClass = optionsContainer.attr('class');

    var optionNumber = new Array();

    // Добавляем в массив классы оставшихся после удаления вариантов
    $('.'+optionsContainerClass+' div[class^=answer-option-number]').each(function () {
        optionNumber.push( $(this).attr('class') );
    });

    //console.log(optionNumber);

    var i=1;

    // Пересчитываем номера оставшихся в результате выполнения функции вариантов ответа
    $.each(optionNumber, function( index, value ) {
        optionsContainer.find('.'+value).html(i+')');
        i++;
    });
};
", \yii\web\View::POS_HEAD );

?>
//$(function() {
//    $('.test-questions-anchors').click(function() {
//        if($('.questions-list-wrapper').css('display') === "none") {
//        	$('.test-question-list > i').attr('class', 'fa fa-arrow-up');
//        	$('.questions-list-header').html('Скрыть список вопросов');
//        } else {
//        	$('.test-question-list > i').attr('class', 'fa fa-arrow-down');
//        	$('.questions-list-header').html('Показать список вопросов');
//        }
//
//        $('.questions-list-wrapper').toggle('slow');
//        
//    });
// });
</script>
    
    <?php echo $this->context->renderPartial('test_form', array('test'=>$test, 'questions'=>$questions, 'pageLabel'=>$pageLabel)); ?>
</div>

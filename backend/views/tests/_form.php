<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
/* @var $this yii\web\View */
/* @var $model common\models\Tests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tests-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form th-test-form">
        <div class="test-fields">
            <div class="row">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])->hint('Название теста',['class' => 'test-form-hint'])->label('Название',['class' => 'test-form-label']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'description')->widget(Widget::className(), [
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 200,
                        'imageUpload' => Url::to(['/default/image-upload']),
                        'plugins' => [
                            'clips',
                            'fullscreen',
                            'imagemanager',
                        ],
                    ],
                ])->hint('Короткое описание теста',['class' => 'test-form-hint'])->label('Описание',['class' => 'test-form-label']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(\common\models\TestsCategory::find()->all(), 'id', 'name'))->hint('Категория теста',['class' => 'test-form-hint'])->label('Категория',['class' => 'test-form-label']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'question_count')->textInput()->hint('Количество вопросов',['class' => 'test-form-hint'])->label('Количество вопросов',['class' => 'test-form-label']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'minimum_score')->textInput()->hint('Минимальный балл для прохождения теста',['class' => 'test-form-hint'])->label('Минимальный балл',['class' => 'test-form-label']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'time_limit')->textInput()->hint('Время на прохождение теста (мин)',['class' => 'test-form-hint'])->label('Время на прохождение теста (мин)',['class' => 'test-form-label']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'attempts')->textInput()->hint('Количество попыток',['class' => 'test-form-hint'])->label('Попыток',['class' => 'test-form-label']) ?>
            </div>
<!--            <div class="row">
                <?= $form->field($model, 'create_time')->textInput()->hint('Вермя создания теста',['class' => 'test-form-hint'])->label('Время создания',['class' => 'test-form-label']) ?>
            </div>-->
            <div class="row">
                <?= $form->field($model, 'deadline')->textInput()->hint('Срок сдачи',['class' => 'test-form-hint'])->label('Срок сдачи',['class' => 'test-form-label']) ?>
            </div>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

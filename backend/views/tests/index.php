<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список тестов';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php echo Yii::$app->session->getFlash('success'); ?>
<div class="tests-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'description',
//            'category_id',
            ['attribute' => 'categoryName','label' => 'Категория'],
            'question_count',
            'minimum_score',
            'time_limit',
            'attempts',
            'create_time',
            'deadline',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

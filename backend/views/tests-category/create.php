<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TestsCategory */

$this->title = 'Create Tests Category';
$this->params['breadcrumbs'][] = ['label' => 'Tests Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tests-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TestsCategory */

$this->title = 'Update Tests Category: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tests Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tests-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

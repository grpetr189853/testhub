<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use kartik\sidenav\SideNav;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Menu;
use backend\assets\FontAwesomeAsset;

FontAwesomeAsset::register($this);
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
<body>
<?php $this->beginBody() ?>
    <div class="container th-container">
        <div class="navbar navbar-default th-navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">

                    <?php
                    NavBar::begin([
                            'options' => [
                              'class' => 'navbar-nav navbat-fixed-top',
                            ],
                    ]);
                    if(Yii::$app->user->isGuest) {
                        $menuItems[] = ['label' => Html::img(Yii::getAlias('@web').'/img/logo.png' ), 'encode' => false,'url' => '/'];
                        $menuItems[] = ['label' => 'Login','url'=>['site/login'],'options' => ['class' => 'menu'],];
                    } else {
                        $menuItems[] = ['label' => Html::img(Yii::getAlias('@web').'/img/logo.png' ), 'encode' => false,'url' => '/'];

                        $menuItems[] = [
                            'label' => 'Последние результаты',
                            'options' => ['class' => 'menu'],

                        ];

                        $menuItems[] = [
                            'label' => 'Пользователи',
                            'items' => [

                                ['label' => 'Список  пользоватлей', 'icon'=>'plus-sign', 'url'=>Url::to(['/site/list-users'])],
                                ['label' => 'Создать пользователя', 'icon'=>'plus-sign', 'url'=> Url::to(['/user/create'])],
                            ],
                            'options' => ['class' => 'menu'],

                        ];
                        $menuItems[] = [

                            'label' => 'Тесты',

                            'items' => [
                                ['label' => 'Список тестов', 'icon'=>'info-sign', 'url'=> Url::to(['/tests/index'])],
                                ['label' => 'Список категорий тестов', 'icon'=>'info-sign', 'url'=> Url::to(['/tests-category/index'])],
                                ['label' => 'Создать тест', 'icon'=>'plus-sign', 'url'=> Url::to(['/tests/create'])],
                                ['label' => 'Создать категорию тестов', 'icon'=>'plus-sign', 'url'=> Url::to(['/tests-category/create'])],
//                                ['label' => 'Создать вопрос', 'icon'=>'question-sign', 'url'=> Url::to(['/tests-questions/create'])],
//                                ['label' => 'Список вопросов', 'icon'=>'info-sign', 'url'=> Url::to(['/tests-questions/index'])],
                                ['label' => 'Назначить тест', 'icon'=>'info-sign', 'url'=> Url::to(['/appoint/appoint'])],
                            ],
                            'options' => ['class' => 'menu'],
                        ];
                        $menuItems[] = '<li class="menu">'
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')',
                                ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>';
                    }


                    echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-left'],
                        'items' => $menuItems,
                    ]);
                    NavBar::end();
                    ?>
                </div>

            </div>
            <!--/.container-fluid-->
        </div>
        <!--/.navbar-->
        <div class="jumbotron th-jumbotron">
            <div class="navbar-collapse collapse menu">
                <div class="icon-close">

                </div>

            </div>

            <?php echo $content; ?>
        </div>
    </div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
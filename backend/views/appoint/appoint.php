<?php


use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Назначить тест';
?>


<div class="appoint-form">

    <?php $form = ActiveForm::begin([
        'id' => 'users-tests-form',
        'options' => ['class' => 'form-horizontal'],
    ]) ?>

    <h1 class="first-header"><?= Html::encode($this->title) ?></h1>

    <div class="form th-appoint-form">
        <div class="appoint-fields">
            <div class="tests-list row">
                <div class="col-lg-5">
                    <?= $form->field($model, 'test_id')->dropDownList(ArrayHelper::map(\common\models\Tests::find()->all(), 'id', 'name'))->label('Выберите тест',[
                        'class' => 'control-label select-label',
                    ]) ?>
                </div>
                <div class="col-lg-offset-1 col-lg-4">
                    <?= $form->field($model, 'user_id')->checkboxList(ArrayHelper::map($students, 'id', 'username'),[
                        'item' => function($index, $label, $name, $checked, $value) {
                            return '<label class="students-checkbox" style="display:block;">
                  <input type="checkbox" name="' . $name . '" value="' . $value . '">
                  <span>' . ucwords($label) . '</span>
               </label>';
                        }
                    ])->label('Выберите пользователя') ?>
                </div>
            </div>
        </div>

    </div>

    <div class="form-group" style="margin-left: 0px;">
        <?= Html::submitButton('Назначить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>




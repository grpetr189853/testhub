<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-list">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'email',
            [
            'attribute' => 'status',
            'filter' => \common\helpers\UserStatusHelper::statusList(),
                'value' => function (common\models\User $model) {
                    return \common\helpers\UserStatusHelper::statusLabel($model->status);
                },
                'format' => 'raw',
            ],

        ],
    ]); ?>


</div>
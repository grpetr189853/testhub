<?php

use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
/* @var $this yii\web\View */
/* @var $model common\models\TestsQuestions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tests-questions-form">
    <?php if( Yii::$app->session->hasFlash('success') ): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif;?>
    <?php $form = ActiveForm::begin(); ?>
    <div class="form th-question-form">
        <div class="test-question-fields">
            <div class="row">
                <?= $form->field($model, 'question_text')->widget(Widget::className(), [
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 200,
                        'imageUpload' => Url::to(['/default/image-upload']),
                        'plugins' => [
                            'clips',
                            'fullscreen',
                            'imagemanager',
                        ],
                    ],
                ])->hint('Текст вопроса')->label('Текст вопроса'); ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'question_type')->dropDownList(ArrayHelper::map(\common\models\TestType::find()->all(), 'id', 'type'))->hint('Тип вопроса')->label('Тип вопроса'); ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'points_question')->textInput()->hint('Балл вопроса')->label('Балл вопроса') ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'test_id')->dropDownList(ArrayHelper::map(\common\models\Tests::find()->all(), 'id', 'name'))->hint('Тест')->label('Тест'); ?>
            </div>
        </div>
    </div>
    <h1 class="first-header">Создать ответы на вопрос</h1>
    <div class="form th-answer-form">
        <div class="test-answer-fields">
            <div class="row">
                <?= $form->field($model, 'TestsAnswers')->widget(MultipleInput::className(), [
                    'max' => 4,
                    'columns' => [
                        [
                            'name'  => 'answer_text',
                            'type'  => \vova07\imperavi\Widget::className(),
                            'title' => 'Answer Test',
                            'headerOptions' => [
//                                'style' => 'width: 250px;',
                                'class' => 'col-lg-11'
                            ],
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'correct',
                            'title' => 'Correct',
                            'headerOptions' => [
                                'class' => 'col-lg-1'
                            ],
                        ]
                    ]
                ]); ?>
            </div>

        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

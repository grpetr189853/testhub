<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TestsQuestions */

$this->title = 'Создать вопрос';
$this->params['breadcrumbs'][] = ['label' => 'Tests Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tests-questions-create">

    <h1 class="first-header"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

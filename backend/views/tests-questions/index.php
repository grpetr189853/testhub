<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TestsQuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы тестов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tests-questions-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'test_id',
            [
                'attribute' => 'question_text',
                'value'=> function($value) {
                    return trim(strip_tags($value->question_text));
                }
            ]
            ,
            'question_type',
            'points_question',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

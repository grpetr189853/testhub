<?php


namespace backend\controllers;

use common\models\Tests;
use common\models\User;
use common\models\UsersTests;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class AppointController extends Controller
{
    public function actionAppoint()
    {
        $model = new UsersTests();
        $tests = Tests::find()->all();
        $students = User::find()->where(['in','type','student'])->all();
        if(Yii::$app->request->isPost  && isset(Yii::$app->request->post()['UsersTests']['test_id']) && isset(Yii::$app->request->post()['UsersTests']['user_id'])){
            $selected_test = Tests::find()->where('id=:testId',['testId'  => Yii::$app->request->post()['UsersTests']['test_id']])->one();                                                                                                                                                                                                                                          $model->test_id = ArrayHelper::getValue(Yii::$app->request->post(),'UsersTests.test_id');
            $selected_users_id  = ArrayHelper::getValue(Yii::$app->request->post(),'UsersTests.user_id');
            foreach ($selected_users_id as $user_id)
            {
                $model->user_id = $user_id;
            }
            $model->attempts = $selected_test->attempts;
            $model->deadline = $selected_test->deadline;
            $model->save();
            Yii::$app->session->setFlash('success', "Тест назначен");
            return $this->redirect(['tests/index']);
        }
        return $this->render('appoint',[
            'model'     => $model,
            'tests'     => $tests,
            'students'  => $students,
        ]);
    }
}
<?php

namespace backend\controllers;

use common\models\TestsAnswers;
use common\models\TestType;
use Yii;
use common\models\Tests;
use backend\models\TestsSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\TestsQuestions;

/**
 * TestsController implements the CRUD actions for Tests model.
 */
class TestsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'path' => '@app/files', // Or absolute path to directory where files are stored.
            ],
            'getRowForm' => [
                'class' => 'backend\components\DynamicTabularForm\actions\GetRowForm',
                'view' => 'question_form',
                'modelClass' => 'common\models\TestsQuestions',
            ],
        ];
    }

    /**
     * Lists all Tests models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tests model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tests model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        $test = new Tests();

        $questions = array();
        $answers = array();
        $test->scenario = 'insert';

        
        if (isset($_POST['Tests'])) {
            $test->attributes = $_POST['Tests'];
            
            if (isset($_POST['TestsQuestions'])) {



                foreach ($_POST['TestsQuestions'] as $key => $value) {
                    $question = new TestsQuestions();
                    $question->scenario = $value['modelScenario'];
                    $question_type = TestType::find()->where('type=:questionType',['questionType'=>$value['modelScenario']])->one()->id;
                    $question->question_type = $question_type;
                    $question->load($value,'');


                    foreach ($_POST['TestsQuestions'][$key]['answerOptionsArray'] as $key_answer => $answer_value) {
                        $answer = new TestsAnswers();
                        $answer->answer_text = $answer_value;
                        $answer->correct = 0;
                        $answers[] = $answer;
                    }
                    foreach ($answers as $key_answer => $answer) {
                        if(in_array( ($key_answer + 1),explode(",",$_POST['TestsQuestions'][$key]['correctAnswers']))) {
                            $answers[$key_answer]->correct = 1;
                        }
                    }
                    $question->testsAnswers = $answers;

                    $questions[] = $question;
                }

            }

            $valid = $test->validate();
            foreach ($questions as $question) {
                $valid = $question->validate() && $valid;
            }

            if ($valid) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $test->save();
                    $test->refresh();
                    
                    foreach ($questions as $question) {
                        $question->test_id = $test->id;
                        $question->save();
                        foreach ($answers as $answer) {
                            $answer->test_id = $test->id;
                            $answer->update();
                        }
                        /*
                        try {
                            foreach ($answers as $answer) {
                                $answer->test_id = $test->id;
                                $answer->question_id = $question->id;
                                $answer->save();
                                $answer->refresh();
                            }
                        } catch (Exception $e) {
                            $transaction->rollback();
                        }
                        */
                    }
                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollback();
                }

                return $this->redirect(array(
                    'tests/view',
                    'id' => $test->id
                ));
            }
        }
       return  $this->render('create', array(
            'test' => $test,
            'questions' => $questions
        ));
        /*
        $model = new Tests();
        $questions = array();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $questions = TestQuestions::find()->where('test_id:=testId',['testId'=>$model->id])->all();
            
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'test' => $model,
            'questions' => $questions,
        ]);
        */
    }

    public function actionOptionField($i, $key, $number)
    {
        $model = new TestsQuestions();

        return $this->renderPartial('new_answer_field', array(
            'model' => $model,
            'i' => $i,
            'number' => $number,
            'key' => $key
        ));
    }

    /**
     * Updates an existing Tests model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tests model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tests::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

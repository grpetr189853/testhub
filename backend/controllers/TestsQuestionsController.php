<?php

namespace backend\controllers;

use common\models\TestsAnswers;
use Yii;
use common\models\TestsQuestions;
use backend\models\TestsQuestionsSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TestsQuestionsController implements the CRUD actions for TestsQuestions model.
 */
class TestsQuestionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'path' => '@app/files', // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Lists all TestsQuestions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestsQuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TestsQuestions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TestsQuestions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TestsQuestions();

        $answer = [new TestsAnswers()];

        if(isset(Yii::$app->request->post()['TestsQuestions']['TestsAnswers'])) {
            $data = Yii::$app->request->post()['TestsQuestions']['TestsAnswers'];
            foreach (array_keys($data) as $index) {
                $answer[$index] = new TestsAnswers();
            }
        }
        $model->testsAnswers = $answer;

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($answer, Yii::$app->request->post()['TestsQuestions'])) {
            foreach ($model->testsAnswers as $testsAnswer){
                $testsAnswer->test_id = $model->test_id;
            }
            $model->save();
            Yii::$app->session->setFlash('success', "Вопрос создан");
//            return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect('create');
        }

        return $this->render('create', [
            'model' => $model,
            'answer' => $answer,
        ]);
    }

    /**
     * Updates an existing TestsQuestions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TestsQuestions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TestsQuestions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TestsQuestions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TestsQuestions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

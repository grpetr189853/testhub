<?php

use yii\db\Migration;

/**
 * Class m200906_152543_create_table_users_answers
 */
class m200906_152543_create_table_users_answers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users_answers}}',[
            'id' => $this->primaryKey(),
            'test_id'     => $this->integer(),
            'question_id' => $this->integer(),
            'user_id'     => $this->integer(),
            'answer_id'   => $this->integer(),
            'result_points' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('index-users_answers-test_id',
            '{{%users_answers}}',
            'test_id');
        $this->addForeignKey('fk-users_answers-tests',
            '{{%users_answers}}',
            'test_id',
            '{{%tests}}',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->createIndex('index-users_answers-question_id',
            '{{%users_answers}}',
            'question_id');
        $this->addForeignKey('fk-users_answers-tests_questions',
            '{{%users_answers}}',
            'question_id',
            '{{%tests_questions}}',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->createIndex('index-users_answers-user_id',
            '{{%users_answers}}',
            'user_id');
        $this->addForeignKey('fk-users_answers-user',
            '{{%users_answers}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->createIndex('index-users_answers-answer_id',
            '{{%users_answers}}',
            'answer_id');
        $this->addForeignKey('fk-users_answers-tests_answers',
            '{{%users_answers}}',
            'answer_id',
            '{{%tests_answers}}',
            'id',
            'CASCADE',
            'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users_answers-tests','{{%users_answers}}');
        $this->dropIndex('index-users_answers-test_id','{{%users_answers}}');
        $this->dropForeignKey('fk-users_answers-tests_questions','{{%users_answers}}');
        $this->dropIndex('index-users_answers-question_id','{{%users_answers}}');
        $this->dropForeignKey('fk-users_answers-user','{{%users_answers}}');
        $this->dropIndex('index-users_answers-user_id','{{%users_answers}}');
        $this->dropForeignKey('fk-users_answers-tests_answers','{{%users_answers}}');
        $this->dropIndex('index-users_answers-answer_id','{{%users_answers}}');
        $this->dropTable('{{%users_answers}}');
    }

}

<?php

use yii\db\Migration;

/**
 * Class m200901_122259_create_table_users_tests
 */
class m200901_122259_create_table_users_tests extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users_tests}}',[
            'id' => $this->primaryKey(),
            'test_id' => $this->integer(),
            'user_id' => $this->integer(),
            'attempts'=> $this->integer(),
            'deadline'=> $this->timestamp(),
            'result'  => $this->integer(),
            'start_time' => $this->timestamp(),
            'end_time'   => $this->timestamp(),
        ], $tableOptions);

        $this->createIndex('index-users_tests-test_id',
            '{{%users_tests}}',
            'test_id');
        $this->addForeignKey(
            'fk-users_tests-tests',
            '{{%users_tests}}',
            'test_id',
            '{{%tests}}',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->createIndex('index-users_tests-user_id',
            '{{%users_tests}}',
            'user_id');
        $this->addForeignKey(
            'fk-users_tests-user',
            '{{%users_tests}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'RESTRICT');

//        $this->execute('ALTER TABLE `users_tests` ADD UNIQUE INDEX `index-users_tests_unique_twice` (`test_id`,`user_id`) USING BTREE;');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users_tests-tests', '{{%users_tests}}');
        $this->dropIndex('index-users_tests-test_id', '{{%users_tests}}');
        $this->dropForeignKey('fk-users_tests-user', '{{%users_tests}}');
        $this->dropIndex('index-users_tests-user_id', '{{%users_tests}}');
        $this->dropIndex('index-users_tests_unique_twice','{{%users_tests}}');
        $this->dropTable('{{%users_tests}}');
    }

}

<?php

use yii\db\Migration;

/**
 * Class m201011_162500_teacher_student_login_via_users_table
 */
class m201011_162500_teacher_student_login_via_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('INSERT INTO public.user (id, username, auth_key, password_hash, password_reset_token, email, status, created_at, updated_at, type, verification_token) VALUES
        (1, \'grpetr189853\', \'KppgskmG-Hksg1t3WhW645iYiJI0aUF_\', \'$2y$13$tudAUjeTZAfmckQH3ztbNuw1Iv8/vvJpBSi40y12LcyzyckFcTRxS\', NULL, \'grpetr189853@gmail.com\', 10, 1596437829, 1596437900, NULL, \'QJJook0IRpkIVQ0Pu9hbF0auvHKaFWFQ_1596437829\'),
        (2, \'teacher\', \'vIFCJsKd8QxzJj5cvZYbbRkpAx6jftj1\', \'$2y$13$FYCiDmBre9sGPcU46P/vhOuQdrwkg8DLVOIQHRGRmLmoQalg8olYi\', NULL, \'teacher189853@gmail.com\', 10, 1598426046, 1598426404, \'teacher\', \'lQI2QH0dI4nzIo2MsCfDZMYZvjDCh4f__1598426046\'),
        (3, \'student\', \'tAuVC_LtT5GOny4-KYkT6ru0y0zlUl18\', \'$2y$13$QsQaAwY5RJvQXO6ua1kD7OxwiLLiyb6kqY/jmdxV8rh41DLsG6chm\', NULL, \'student189853@gmail.com\', 10, 1598426521, 1598427070, \'student\', \'6ELNUPnDbnz6or4NZGdlLthraaIwEPg-_1598426521\'),
        (9, \'student_2\', \'ePnf6w_m3VMyfgMM_njDgSJWWvBgg82e\', \'$2y$13$XGv0DHF5uBDMgKkpMRpsAOU85pscDpS/AHUkx9a3RBdU3V7JZ1XzO\', NULL, \'student189853_2@gmail.com\', 10, 1599046763, 1599046869, NULL, \'q55CW9qKF4bgYm6ZSntbUYb5aqNom7Bp_1599046763\');
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201011_162500_teacher_student_login_via_users_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201011_162500_teacher_student_login_via_users_table cannot be reverted.\n";

        return false;
    }
    */
}

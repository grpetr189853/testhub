<?php

use yii\db\Migration;

/**
 * Class m200802_115016_create_tests_category_tests_tests_questions_tests_answers_test_type_tables_and_keys
 */
class m200802_115016_create_tests_category_tests_tests_questions_tests_answers_test_type_tables_and_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tests_category}}',[
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
        ]);
        $this->createTable('{{%tests}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
            'description'   => $this->string(),
            'category_id'   => $this->integer(),
            'question_count'=> $this->integer(),
            'minimum_score' => $this->integer(),
            'time_limit'    => $this->integer(),
            'attempts'      => $this->integer(),
            'create_time'   => $this->timestamp(),
            'deadline'      => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%tests_questions}}', [
            'id'            => $this->primaryKey(),
            'test_id'       => $this->integer(),
            'question_text' => $this->text()->notNull(),
            'question_type' => $this->integer(),
            'points_question'=> $this->integer()->notNull(),
            'testAnswersJson'=> $this->json(),
        ], $tableOptions);

        $this->createTable('{{%tests_answers}}',[
            'id'            => $this->primaryKey(),
            'test_id'       => $this->integer(),
            'answer_text'   => $this->string()->notNull(),
            'correct'       => $this->integer()->notNull(),
            'question_id'   => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%test_type}}', [
            'id'            => $this->primaryKey(),
            'type'          => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'index-tests-category_id',
            '{{%tests}}',
            'category_id');
        $this->addForeignKey(
            'fk_tests-tests_category',
            '{{%tests}}',
            'category_id',
            '{{%tests_category}}',
            'id',
            'CASCADE',
            'RESTRICT');


        $this->createIndex('index-tests_questions-test_id',
            '{{%tests_questions}}',
            'test_id');
        $this->addForeignKey(
            'fk-tests_questions-tests',
            '{{%tests_questions}}',
            'test_id',
            '{{%tests}}',
            'id',
            'CASCADE',
            'RESTRICT');
        $this->createIndex('index-tests_questions-question_type',
            '{{%tests_questions}}',
            'question_type');
        $this->addForeignKey(
            'fk-tests_questions-test_type',
            '{{%tests_questions}}',
            'question_type',
            'test_type',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->createIndex('index-tests_answers-test_id',
            '{{%tests_answers}}',
            'test_id');
        $this->addForeignKey('fk-tests_answers-tests',
            '{{%tests_answers}}',
            'test_id',
            '{{%tests}}',
            'id',
            'CASCADE',
            'RESTRICT');
        $this->createIndex('index-tests_answers-question_id',
            '{{%tests_answers}}',
            'question_id');
        $this->addForeignKey('fk_tests_a-tests_questions',
            '{{%tests_answers}}',
            'question_id',
            '{{%tests_questions}}',
            'id',
            'CASCADE',
            'RESTRICT');


        $this->batchInsert('{{%tests_category}}',['id','name'],[
            [1,'frontend'],
            [2,'backend'],
            [3,'general'],
        ]);
        $this->batchInsert('{{%tests}}',['id','name','description','category_id','question_count','minimum_score','time_limit','attempts'],[
            [1,'javascript','javascript basic test', 1, 5, 5, 5, 5],
            [2,'php','php basic test',2,2,2,5,5],
            [3,'general_text','general_text',3,2,2,5,5],
            [4,'general_numeric','general_numeric',3,2,2,5,5],

        ]);
        $this->batchInsert('{{%test_type}}',['id','type'],[
            [1,'select_one'],
            [2,'select_many'],
            [3,'numeric'],
            [4,'string'],
        ]);
        $this->batchInsert('{{%tests_questions}}',['id','test_id','question_text','question_type','points_question'],[
            [1,1,'После какого HTML тега нужно вставить JavaScript?',1,1],
            [2,1,'JavaScript и Java это одно и тоже?',1,1],
            [3,1,'В каком разделе можно вставить JavaScript?',1,1],
            [4,1,'Внешний JavaScript должен содержать script тег?',1,1],
            [5,1,'Как можно вывести сообщение "Hello World" с помощью JavaScript?',1,1],
            [6,2,'Выберите поддерживаемые версии PHP',2,1],
            [7,2,'Выберите типы ошибок в PHP',2,1],
            [8,3,'Столица Украины',4,1],
            [9,3,'Столица России',4,1],
            [10,4,'2+2',3,1],
            [11,4,'3+3',3,1],
        ]);
        $this->batchInsert('{{%tests_answers}}',['id','test_id','answer_text','correct','question_id'],[
            [1,1,'javascript',0,1],
            [2,1,'script',1,1],
            [3,1,'scripting',0,1],
            [4,1,'js',0,1],
            [5,1,'Да',0,2],
            [6,1,'Нет',1,2],
            [7,1,'head и body',1,3],
            [8,1,'Ни одно из перечисленного',0,3],
            [9,1,'body',0,3],
            [10,1,'head',0,3],
            [11,1,'Да',0,4],
            [12,1,'Нет',1,4],
            [13,1,'alertBox("Hello World");',0,5],
            [14,1,'msg("Hello World");',0,5],
            [15,1,'alert("Hello World");',1,5],
            [16,1,'msgBox("Hello World");',0,5],
            [17,2,'5.6',1,6],
            [18,2,'7',1,6],
            [19,2,'7.1',1,6],
            [20,2,'7.2',1,6],
            [21,2,'Замечания',1,7],
            [22,2,'Предупреждения',1,7],
            [23,2,'Фатальные ошибки',1,7],
            [24,3,'Киев',1,8],
            [25,3,'Москва',1,9],
            [26,4,'4',1,10],
            [27,4,'6',1,11],
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \Yii::$app->db->createCommand()->delete('{{%tests_answers}}', ['in', 'id', [
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27
            ]]
        )->execute();
        \Yii::$app->db->createCommand()->delete('{{%tests_questions}}', ['in', 'id', [
                1, 2, 3, 4, 5, 6 , 7, 8, 9, 10, 11
            ]]
        )->execute();
        \Yii::$app->db->createCommand()->delete('{{%test_type}}', ['in', 'id', [
                1, 2, 3, 4
            ]]
        )->execute();
        \Yii::$app->db->createCommand()->delete('{{%tests}}', ['in', 'id', [
                1,2,3,4
            ]]
        )->execute();
        \Yii::$app->db->createCommand()->delete('{{%tests_category}}', ['in', 'id', [
                1,2,3
            ]]
        )->execute();

        $this->dropForeignKey('fk-tests_questions-tests', '{{%tests_questions}}');
        $this->dropIndex('index-tests_questions-test_id', '{{%tests_questions}}');

        $this->dropForeignKey('fk-tests_questions-test_type', '{{%tests_questions}}');
        $this->dropIndex('index-tests_questions-question_type', '{{%tests_questions}}');

        $this->dropForeignKey('fk-tests_answers-tests', '{{%tests_answers}}');
        $this->dropIndex('index-tests_answers-test_id', '{{%tests_answers}}');

        $this->dropForeignKey('fk_tests_a-tests_questions', '{{%tests_answers}}');
        $this->dropIndex('index-tests_answers-question_id', '{{%tests_answers}}');

        $this->dropForeignKey('fk_tests-tests_category', '{{%tests}}');

        $this->dropTable('{{%tests}}');
        $this->dropTable('{{%tests_category}}');
        $this->dropTable('{{%tests_questions}}');
        $this->dropTable('{{%tests_answers}}');
        $this->dropTable('{{%test_type}}');

    }
}

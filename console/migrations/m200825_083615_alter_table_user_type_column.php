<?php

use yii\db\Migration;

/**
 * Class m200825_083615_alter_table_user_type_column
 */
class m200825_083615_alter_table_user_type_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->addColumn('{{%user}}', 'type', 'TEXT  AFTER updated_at');
        $this->execute("ALTER TABLE public.user ADD COLUMN type VARCHAR(255);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}','type');
    }

}

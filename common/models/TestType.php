<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "test_type".
 *
 * @property int $id
 * @property string $type
 *
 * @property TestsQuestions[] $testsQuestions
 */
class TestType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
        ];
    }

    /**
     * Gets query for [[TestsQuestions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestsQuestions()
    {
        return $this->hasMany(TestsQuestions::className(), ['question_type' => 'id']);
    }
}

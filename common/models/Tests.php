<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tests".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int|null $category_id
 * @property int|null $question_count
 * @property int|null $minimum_score
 * @property int|null $time_limit
 * @property int|null $attempts
 * @property string|null $create_time
 * @property string|null $deadline
 *
 * @property TestsCategory $category
 * @property TestsAnswers[] $testsAnswers
 * @property TestsQuestions[] $testsQuestions
 */
class Tests extends \yii\db\ActiveRecord
{
    
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'question_count', 'minimum_score', 'time_limit', 'attempts'], 'required'],
            [['category_id', 'question_count', 'minimum_score', 'time_limit', 'attempts'], 'integer'],
            [['create_time', 'deadline'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestsCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_INSERT => ['name', 'category_id', 'question_count', 'minimum_score', 'time_limit', 'attempts','description'],
            self::SCENARIO_UPDATE => ['name', 'category_id', 'question_count', 'minimum_score', 'time_limit', 'attempts','description'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'category_id' => 'Category ID',
            'question_count' => 'Question Count',
            'minimum_score' => 'Minimum Score',
            'time_limit' => 'Time Limit',
            'attempts' => 'Attempts',
            'create_time' => 'Create Time',
            'deadline' => 'Deadline',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TestsCategory::className(), ['id' => 'category_id']);
    }

    public function getCategoryName() {
        return $this->category->name;
    }

    /**
     * Gets query for [[TestsAnswers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestsAnswers()
    {
        return $this->hasMany(TestsAnswers::className(), ['test_id' => 'id']);
    }

    /**
     * Gets query for [[TestsQuestions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestsQuestions()
    {
        return $this->hasMany(TestsQuestions::className(), ['test_id' => 'id']);
    }

    /**
     * The method returns an array of test questions IDs that the student has already answered
     * @param $questionNumberIdPair array
     * @return array
     */
    public static function getStudentAnswersByQuestionsId($questionNumberIdPair)
    {
        $usersAnswers = UsersAnswers::find()->where(['in', 'question_id', $questionNumberIdPair])->all();

        $studentAnswersQuestionId = array();
        foreach ($usersAnswers as $userAnswer){
            $studentAnswersQuestionId[] = $userAnswer->question_id;
        }

        return $studentAnswersQuestionId;
    }

    /**
     * The method will return false if the test execution timed out
     * @param $startTime
     * @param $timeLimit
     * @return bool
     */
    public function checkTestTimeLimit($startTime, $timeLimit)
    {
        if (time() >= ($startTime + $timeLimit)) {
            return false;
        }

        return true;
    }
}

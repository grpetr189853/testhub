<?php

namespace common\models;

use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;

/**
 * This is the model class for table "tests_questions".
 *
 * @property int $id
 * @property int|null $test_id
 * @property string $question_text
 * @property int|null $question_type
 * @property int $points_question
 * @property string|null $testAnswersJson
 *
 * @property TestsAnswers[] $testsAnswers
 * @property TestType $questionType
 * @property Tests $test
 */
class TestsQuestions extends \yii\db\ActiveRecord
{
    public $updateType;
    
    public $modelScenario;

    public $answerOptionsArray;

    public $answerIdTextPair;
    
    public $questionNumber;
    
    public $optionsNumber = array(
        1,
        2
    );

    const SCENARIO_SELECT_ONE = 'select_one';

    const SCENARIO_SELECT_MANY = 'select_many';
    
    const TYPE_ONE = 'select_one';

    const TYPE_MANY = 'select_many';

    const TYPE_STRING = 'string';

    const TYPE_NUMERIC = 'numeric';

    public $correctAnswers;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tests_questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_id', 'question_type', 'points_question'], 'integer'],
            [['question_text', 'points_question'], 'required'],
            [['question_text', 'points_question','testAnswersJson'], 'safe'],
            [['question_text'], 'string', 'max' => 65535],
            [['question_type'], 'exist', 'skipOnError' => true, 'targetClass' => TestType::className(), 'targetAttribute' => ['question_type' => 'id']],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tests::className(), 'targetAttribute' => ['test_id' => 'id']],
        ];
    }
    
    public function scenarios()
    {
        return [
            self::SCENARIO_SELECT_MANY => ['test_id', 'question_type', 'points_question', 'testAnswers', 'question_type','question_text'],
            self::SCENARIO_SELECT_ONE => ['test_id', 'question_type', 'points_question', 'testAnswers', 'question_type','question_text'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['testsAnswers']
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'question_text' => 'Question Text',
            'question_type' => 'Question Type',
            'points_question' => 'Points Question',
            'testAnswersJson' => 'Test Answers Json',
        ];
    }

    /**
     * Gets query for [[TestsAnswers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestsAnswers()
    {
        return $this->hasMany(TestsAnswers::className(), ['question_id' => 'id']);
    }

    /**
     * Gets query for [[QuestionType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionType()
    {
        return $this->hasOne(TestType::className(), ['id' => 'question_type']);
    }

    /**
     * Gets query for [[Test]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Tests::className(), ['id' => 'test_id']);
    }

    public function validateAnswerOptions()
    {
        if (is_array($this->answerOptionsArray)) {
            foreach ($this->answerOptionsArray as $optionNumber => $optionText) {
                if ($optionText === '') {
                    $this->addError("answerOptionsArray[{$optionNumber}]", 'Поле не должно быть пустым');
                }
            }
        } else {
            if ($this->answerOptionsArray === '') {
                $this->addError("answerOptionsArray", 'Поле не должно быть пустым');
            }
        }
    }

    public function formatCorrectAnswers()
    {
        if (! preg_match_all('/\d+/', $this->correctAnswers, $correctAnswersArray)) {
            $this->addError('correctAnswers', 'Необходимо указать номера правильных ответов из вышепреведенных вариантов. Если ответов несколько, разделите их запятой: 1, 3');
        }
        
        if (is_array($this->answerOptionsArray)) {
            foreach ($correctAnswersArray[0] as $correctNumber) {
                if ($correctNumber == 0 || $correctNumber > count($this->answerOptionsArray)) {
                    $this->addError('correctAnswers', 'Вариант с номером "' . $correctNumber . '" не найден.');
                }
            }
        }
        
        $this->correctAnswers = implode(', ', $correctAnswersArray[0]);
    }
    /*
    public function afterFind()
    {
        parent::afterFind();
        
        switch ($this->type) {
            case self::TYPE_ONE:
                $this->scenario = 'select';
                break;
            case self::TYPE_MANY:
                $this->scenario = 'select';
                break;
            case self::TYPE_STRING:
                $this->scenario = 'string';
                break;
            case self::TYPE_NUMERIC:
                $this->scenario = 'numeric';
                break;
            default:
                $this->scenario = '';
                break;
        }
        
        $this->optionsNumber = array();
        
        foreach ($this->answerOptions as $answerOption) {
            $this->answerOptionsArray[$answerOption->option_number] = $answerOption->option_text;
            $this->optionsNumber[] = $answerOption->option_number;
            $this->answerIdTextPair[$answerOption->id] = $answerOption->option_text;
            
            if ($this->type === 'select_one' && $answerOption->id == $this->answer_id) {
                $this->correctAnswers = $answerOption->option_number;
            }
        }
        
        if ($this->type === 'select_many') {
            $correctAnswersArray = array();
            
            foreach ($this->correctAnswer1 as $correctAnswer) {
                $correctAnswersArray[] = $correctAnswer->option_number;
            }
            
            $this->correctAnswers = implode(', ', $correctAnswersArray);
        }
        
        if ($this->type === 'numeric') {
            $this->answer_number = floatval($this->answer_number);
            $this->precision_percent = sprintf('%.'.self::PRECISION_D.'f', floatval($this->precision_percent));
        }
    }
    */
     public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->correctAnswers = explode(', ', $this->correctAnswers);
            
            switch ($this->scenario) {
                case 'string':
                    $this->type = self::TYPE_STRING;
                    break;
                case 'numeric':
                    $this->type = self::TYPE_NUMERIC;
                    break;
                default:
                    $type = '';
                    break;
            }
            /*
            if ($this->scenario === 'select') {
                if (count($this->correctAnswers) > 1) {
                    $this->type = self::TYPE_MANY;
                } else {
                    $this->type = self::TYPE_ONE;
                }
            }
            */
            /*
            if ($this->scenario === 'numeric' && isset($this->precision_percent) == false) {
                $this->precision_percent = self::DEFAULT_PRECISION;
            }
            */
//            $this->title = SHelper::purifyHtml($this->title);
            
            return true;
        }
        
        return false;
    }

}

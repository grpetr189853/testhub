<?php

namespace common\models;

use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "users_answers".
 *
 * @property int $id
 * @property int|null $test_id
 * @property int|null $question_id
 * @property int|null $user_id
 * @property int|null $answer_id
 * @property int|null $result_points
 *
 * @property Tests $test
 * @property TestsAnswers $answer
 * @property TestsQuestions $question
 * @property User $user
 */
class UsersAnswers extends \yii\db\ActiveRecord
{

    const SCENARIO_TEXT = 'string';
    const SCENARIO_NUMBER = 'numeric';
    const SCENARIO_SELECT_MANY = 'select_many';
    const SCENARIO_SELECT_ONE = 'select_one';
    const NUMBER_M = 11;
    const NUMBER_D = 4;
    public $selectedAnswers;
    public $answer_text;
    public $answer_number;
    const delta = 0.000001;
    private $requiredMessage = 'Вы не ответили на вопрос';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_id', 'question_id', 'user_id', 'answer_id', 'result_points'], 'integer'],
            [['answer_text'], 'string', 'max' => 200],
            [['answer_number'],'number','integerOnly' => false],
            [['answer_number'],'number','numberPattern' => '/^-?\d{1,' . self::NUMBER_M . '}\.?\d{0,' . self::NUMBER_D . '}$/','message' => 'Число должно быть либо целым, либо с плавающей точкой. Максимум знаков относительно запятой: ' . self::NUMBER_M . ' — перед запятой, ' . self::NUMBER_D . ' — после.'],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tests::className(), 'targetAttribute' => ['test_id' => 'id']],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestsAnswers::className(), 'targetAttribute' => ['answer_id' => 'id'],'on' => self::SCENARIO_SELECT_ONE],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestsQuestions::className(), 'targetAttribute' => ['question_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['answer_text'],'required','on'=>self::SCENARIO_TEXT],
            [['answer_number'],'required','on'=>self::SCENARIO_NUMBER],
            [['answer_id'],'required','on'=>self::SCENARIO_SELECT_ONE],
            [['selectedAnswers'],'required','on'=>self::SCENARIO_SELECT_MANY],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['answers']
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_SELECT_ONE] = ['test_id','question_id','answer_id','result_points'];
        $scenarios[self::SCENARIO_SELECT_MANY] = ['test_id','question_id','result_points','selectedAnswers'];
        $scenarios[self::SCENARIO_TEXT] = ['test_id','question_id','result_points','answer_text'];
        $scenarios[self::SCENARIO_NUMBER] = ['test_id','question_id','result_points','answer_number'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'question_id' => 'Question ID',
            'user_id' => 'User ID',
            'answer_id' => 'Answer ID',
            'result_points' => 'Result Points',
        ];
    }

    /**
     * Gets query for [[Test]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Tests::className(), ['id' => 'test_id']);
    }

    /**
     * Gets query for [[Answer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(TestsAnswers::className(), ['id' => 'answer_id']);
    }

    /**
     * Gets query for [[Answer]] for scenario - select_many.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(TestsAnswers::className(), ['id' => 'selectedAnswers']);
    }


    /**
     * Gets query for [[Question]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(TestsQuestions::className(), ['id' => 'question_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterFind()
    {
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $testResult = (new UsersTests())->find()->where('test_id=:testId AND user_id=:userId', [
                    ':testId' => $this->test_id,
                    ':userId' => $this->user_id,
                ])->one();

//                $this->test_result = $testResult->id;
            }

            $this->compareAnswer();

            return true;
        }

        return false;
    }

    /**
     * Compares answers for different scenarios
     */
    public function compareAnswer()
    {
        $this->result_points = 0;

        if ($this->scenario == self::SCENARIO_SELECT_ONE) {
            if ($this->getAnswer()->one()->correct) {
                $this->result_points = TestsAnswers::CORRECT_ANSWER;
            } else {
                $this->result_points = TestsAnswers::INCORRECT_ANSWER;
            }
        }

        if ($this->scenario == self::SCENARIO_SELECT_MANY) {
            $correctOptionsId = array();
            $correctAnswersArray = TestsAnswers::find()->where('question_id=:questionId AND test_id=:testId',[
                ':questionId' => $this->question_id,
                ':testId' => $this->test_id,
            ])->all();
            foreach ($correctAnswersArray as $optionObject) {
                if($optionObject->correct === TestsAnswers::CORRECT_ANSWER){
                    $this->answer_id = $optionObject->id;
                    $correctOptionsId[] = $optionObject->id;
                }

            }

            if (count($this->selectedAnswers) == count($correctOptionsId) && array_diff($correctOptionsId, $this->selectedAnswers) == false) {
                $this->result_points = TestsAnswers::CORRECT_ANSWER;
            }
        }

        if ($this->scenario == self::SCENARIO_NUMBER) {
            if (abs(((int)($this->question->testsAnswers[0])->answer_text - (int)$this->answer_number)) <= self::delta) {
                $this->result_points = TestsAnswers::CORRECT_ANSWER;
            }
        }

        if ($this->scenario == self::SCENARIO_TEXT) {
            $answer = array(
                'correct' => mb_strtolower(($this->question->testsAnswers)[0]->answer_text),
                'studentAnswer' => mb_strtolower($this->answer_text)
            );

            $formatAnswer = str_replace(' ', '', $answer);

            if ($formatAnswer['correct'] === $formatAnswer['studentAnswer']) {
                $this->result_points = TestsAnswers::CORRECT_ANSWER;
            }
        }
    }

    /**
     * Depending on whether there is an answer to the question with ID $ questionId, we return the response model.
     * @param $questionId integer
     * @param $questionType string
     * @param $testId integer
     * @return mixed
     */

    public function getAnswerModel($questionId, $questionType, $testId)
    {
        $studentCurrentAnswer = $this->find()->where('question_id=:questionId AND user_id=:userId', [
            ':questionId' => $questionId,
            ':userId' => Yii::$app->user->getId(),
        ])->one();

        if ($studentCurrentAnswer) {
            $answerModel = $studentCurrentAnswer;
            $answerModel->scenario = $questionType;
        } else {
            $answerModel = new UsersAnswers();
            $answerModel->scenario = $questionType;
            $answerModel->test_id = $testId;
            $answerModel->user_id = Yii::$app->user->getId();
        }

        return $answerModel;
    }



}

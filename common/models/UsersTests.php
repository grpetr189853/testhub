<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users_tests".
 *
 * @property int $id
 * @property int|null $test_id
 * @property int|null $user_id
 * @property int|null $attempts
 * @property string|null $deadline
 * @property int|null $result
 * @property string|null $start_time
 * @property string|null $end_time
 *
 * @property Tests $test
 * @property User $user
 */
class UsersTests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_tests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_id', 'user_id', 'attempts', 'result'], 'integer'],
            [['deadline', 'start_time', 'end_time'], 'safe'],
            [['test_id', 'user_id'], 'unique', 'targetAttribute' => ['test_id', 'user_id']],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tests::className(), 'targetAttribute' => ['test_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'user_id' => 'User ID',
            'attempts' => 'Attempts',
            'deadline' => 'Deadline',
            'result' => 'Result',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
        ];
    }

    /**
     * Gets query for [[Test]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Tests::className(), ['id' => 'test_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

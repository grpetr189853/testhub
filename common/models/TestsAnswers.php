<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tests_answers".
 *
 * @property int $id
 * @property int|null $test_id
 * @property string $answer_text
 * @property int $correct
 * @property int $question_id
 *
 * @property Tests $test
 * @property TestsQuestions $question
 */
class TestsAnswers extends \yii\db\ActiveRecord
{
    const INCORRECT_ANSWER = 0;
    const CORRECT_ANSWER = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tests_answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_id', 'correct', 'question_id'], 'integer'],
//            [['answer_text', 'correct', 'question_id'], 'required'],
            [['answer_text', 'correct'], 'required'],
            [['answer_text'], 'string', 'max' => 255],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tests::className(), 'targetAttribute' => ['test_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestsQuestions::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'answer_text' => 'Answer Text',
            'correct' => 'Correct',
            'question_id' => 'Question ID',
        ];
    }

    /**
     * Gets query for [[Test]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Tests::className(), ['id' => 'test_id']);
    }

    /**
     * Gets query for [[Question]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(TestsQuestions::className(), ['id' => 'question_id']);
    }
    /**
     * Return the correct answer id
     * @param integer $questionId
     * @param integer $testId
     * @return integer
     */
    public static function getCorrectAnswerId($questionId, $testId)
    {
        $userCurrentAnswer = TestsAnswers::find()->where('question_id=:questionId  AND test_id=:testId',[
            'questionId' => $questionId,
            'testId'     => $testId,
        ])->all();

        foreach ($userCurrentAnswer as $answer)
        {
            if($answer->correct === self::CORRECT_ANSWER){
                return $answer->id;
            }
        }
    }
}
